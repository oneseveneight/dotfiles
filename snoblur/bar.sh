#!/bin/bash

# Options
width="1920"
height="10"
font="monospace-9"
ssid="nothing"

# Get monitor width so we can center the bar.
resolution="$(xrandr --nograb --current | awk '/\*/ {printf $1; exit}')"
monitor_width="${resolution/x*}"
offset="$((monitor_width / 2 - width / 2))"
geometry="${width}x${height}+0+5"

Clock() {
    DATETIME=$(date "+%a %b %d %Y   %I:%M %p")

    echo -n "$DATETIME"
}

Groups() {
	xprop -root WINDOWCHEF_ACTIVE_GROUPS | cut -f 3- -d ' ' |tr -d ' '| tr ',' '|'
}

Battery() {
    BATPERC=$(acpi --battery | cut -d, -f2)
    echo "$BATPERC"
}

Remaining() {
	echo "$(df -h | grep /dev/sda4 | tr -d ' ' | cut -f 3 -d 'G')G"
}

Wifi() {
    ssid=$(iwconfig wlp4s0 |grep ESSID| awk '{print $NF}'|cut -d ":" -f 2)
    ip=$(ifconfig wlp4s0|grep 10|grep 'inet addr')
    ip=$(echo $ip|cut -d " " -f 2 |awk '{gsub("addr:", "");print}')
    echo -n "$ssid" | tr -d '"'
    return
}
NowPlaying(){
    SONG=$(dbus-send --print-reply --session --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.freedesktop.DBus.Properties.Get string:'org.mpris.MediaPlayer2.Player' string:'Metadata' | grep title -A 1 |tail -n 1 |cut -c 43-)

    ARTIST=$(dbus-send --print-reply --session --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.freedesktop.DBus.Properties.Get string:'org.mpris.MediaPlayer2.Player' string:'Metadata' | grep albumArtist -A 2 | tail -n 1 | cut -c 26- | awk '{gsub("\"", "");print}')

    echo "$ARTIST - $SONG"
}

while true; do
    echo " $(Groups)%{r}%{F#ffffff}%{B#00000000} / $(Remaining)B  $(Clock) %{F-}%{B-}"
    sleep 0.1
done | lemonbar -g $geometry -f "$font" -B "#00000000"
