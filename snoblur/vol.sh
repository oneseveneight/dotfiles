#!/bin/bash
width="175"
height="20"
geometry="${width}x${height}+845+520"
 
Volume(){
    volume=$(pulseaudio-ctl |grep "Volume level" \
    |sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[mGK]//g" \
    |gawk '{print $4}')
    
	echo $volume | SIZE=15 START="" END="$volume" SEP="|" CHAR1="-" CHAR2=" " mkb
}
 
while true; do
xdotool windowclose $(xwininfo -name volumebar | grep "Window id" | cut -f 4 -d ' ')
echo -e "%{l}%{F#ffffff}%{B#00fafafa} \uf028 $(Volume) %{F-}%{B-}" && sleep 1 && exit
done | lemonbar -g $geometry -f "DejaVu Sans Mono-9" -B "#00fafafa" -n "volumebar"
