#!/bin/sh
CURR_BLUR=$(cat ~/.config/windowchef/blur_strength)

if [ "$1" = "up" ]
then
	NEW_TRANS=$(( $(cat ~/.config/windowchef/transparency) +5 ))
elif [ "$1" = "down" ]
then
	NEW_TRANS=$(( $(cat ~/.config/windowchef/transparency) -5 ))
fi
sed -i 's/[0-9]\+:class_g/'$NEW_TRANS':class_g/' ~/.config/compton.conf 
killall compton
compton --blur-method kawase --blur-strength "$CURR_BLUR" & \
echo "$NEW_TRANS" > ~/.config/windowchef/transparency
