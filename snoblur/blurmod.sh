#!/bin/sh
if [ "$1" = "up" ]
then
	NEW_BLUR=$(( $(cat ~/.config/windowchef/blur_strength) +1 ))
elif [ "$1" = "down" ]
then
	NEW_BLUR=$(( $(cat ~/.config/windowchef/blur_strength) -1 ))
fi

killall compton
compton --blur-method kawase --blur-strength $NEW_BLUR \
	& echo $NEW_BLUR > ~/.config/windowchef/blur_strength
