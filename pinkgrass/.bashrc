# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything

PATH=$PATH:/$HOME/.local/bin

case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
#[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

#PS1='\[\033[07m\]${debian_chroot:+($debian_chroot)} \u@\h:\w\$ '

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# color aliases
alias ls='ls --color=auto'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'

alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

bandcamp_fix() {
    for f in *; do mv "$f" "$(echo "$f" | sed 's/^.*- //')"; done
}

wpa() {
	sudo killall wpa_supplicant
	sudo killall dhcpcd
	sudo wpa_supplicant -B -Dnl80211 -iwlp5s0 -c /etc/wpa_supplicant/wpa_supplicant.conf
	sudo dhcpcd
}
eip() {
	curl icanhazip.com
}


alias pdf="sw zathura"
alias bgf="sed 's/\x1B\[[0-9;]\{1,\}[A-Za-z]//g'"

play() {
    trackinfo="$(ffprobe "$1" 2>&1)"
    echo "$trackinfo" | grep -im1 TITLE | tr '[:lower:]' '[:upper:]' > ~/.nowplaying
    echo "$trackinfo" | grep -im1 ARTIST | tr '[:lower:]' '[:upper:]' >> ~/.nowplaying
    echo "$trackinfo" | grep -im1 ALBUM | tr '[:lower:]' '[:upper:]' >> ~/.nowplaying
    sed -i 's/^[[:space:]]*//g' ~/.nowplaying
    sed -i 's/[[:space:]]*: /="/g' ~/.nowplaying
    sed -i 's/$/"/g' ~/.nowplaying
    mpv "$1" --no-video | bgf
}

#alias bgf="sed 's/\x1B\[27m\]//g'"

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

PS1="home:\w\a> \[\e[0m\]"
PROMPT_COMMAND='echo -ne "\033]0;bash: $(pwd)\007"'
trap 'printf "\033]0;%s\007" "${BASH_COMMAND//[^[:print:]]/}"' DEBUG

source "$HOME/.cargo/env"
( cat ~/.cache/wal/sequences &)
