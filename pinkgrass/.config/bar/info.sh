#!/bin/sh
. ~/.cache/wal/colors.sh
top_x=5

stats="$(
        top -b -n 10 \
        | head -n $(( 7 + top_x ))
)"

present_day() {
    date +"%A, %B %e, %Y"
}
present_time() {
    date +"%_I:%M %p"
}
vpn_status() {
    if [ -e /proc/sys/net/ipv4/conf/tun0 ]
    then
        echo "on"
    else
        echo "off"
    fi
}

cpu_stats() {
    cpu_usage="$(
        echo "$stats" \
        | head -n 5 \
        | grep -oP ":.*?us," \
        | sed 'y/:,us/    /' \
        | tr -d ' '
    )%"
    cpu_temp="$(
        sensors \
        | grep "Package id" \
        | cut -f 5 -d ' ' \
        | cut -c 2-
    )"
    echo "$cpu_temp, $cpu_usage usage"
}
mem_stats() {
    mem_total_kb="$(
        grep "MemTotal" /proc/meminfo \
        | rev \
        | cut -f 2 -d ' ' \
        | rev
    )"
    mem_avail_kb="$(
        grep "MemAvailable" /proc/meminfo \
        | rev \
        | cut -f 2 -d ' ' \
        | rev
    )"
    mem_avail_gb="$(
        echo "scale=2; $mem_avail_kb / 1000000" \
        | bc -l
    )"

    mem_total_gb="$(
        echo "scale=2; $mem_total_kb / 1000000" \
        | bc -l
    )"
    echo "$mem_avail_gb GB / $mem_total_gb GB"
}
top_procs() {
    # i should probably learn awk
    proc_list="$(
        echo "$stats" \
        | tail -n $top_x \
        | tr '[:upper:]' '[:lower:]' \
        | grep -oP " +.*? ([a-z_-]|[0-9])*" \
        | sed 's/\(\.[0-9][0-9]\) \([-_a-z]*\)/\1\n\2\n|/' \
        | tr -d ' ' \
        | tr '\n' '^' \
        | sed 's/^[0-9]*//g' \
        | sed 's/|^[0-9]*/|/g' \
        | tr '|' '\n' \
        | rev \
        | cut -f 2,4-5 -d '^'\
        | rev \
        | sed 's/\^/ /g'
    )"
    echo "%CPU %RAM Command\n$proc_list" | column -t
}

echo "<span 
      foreground='$color2'
      size='xx-large'
      >  $(present_time)</span>"
echo "$(present_day)"
echo ""
echo "<b><span 
      foreground='$color13'
      >VPN:</span></b> $(vpn_status)"
echo "<b><span 
      foreground='$color6'
      >CPU:</span></b> $(cpu_stats)"
echo "<b><span 
      foreground='$color9'
      >RAM:</span></b> $(mem_stats)"
printf "\n<span foreground='$color4'>"
printf '<b>top</b><s>%*s</s>\n' 22 ""
printf "</span>"
#printf '%*s\n' 35 "-"
echo "$(top_procs)"
