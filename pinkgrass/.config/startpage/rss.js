function add_feed(target_el, feed_url, feed_title) {
    let feed_xhttp = new XMLHttpRequest()
    feed_xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let feed_xml = feed_xhttp.responseText
            let parser = new DOMParser()
            let feed_data = parser.parseFromString(feed_xml, "text/xml")
            let news_items = feed_data.getElementsByTagName("item")

            let feed_container = document.createElement("div")
            let feed_title_el = document.createElement("h2")
            feed_container.className = "info-block scrollable rss-feed"

            feed_title_el.className = "minor-title rss"
            feed_title_el.innerText = feed_title
            feed_container.appendChild(feed_title_el)

            for (const news_item of news_items) {
                let title = news_item.querySelector("title").innerHTML
                let link = news_item.querySelector("link").innerHTML
                let item_el = document.createElement("div")
                let item_link_el = document.createElement("a")
                item_el.className = "rss-item-title"
                item_el.innerText = title
                item_link_el.href = link
                item_link_el.className = "rss-link"

                item_link_el.appendChild(item_el)
                feed_container.appendChild(item_link_el)
            }
            target_el.appendChild(feed_container)
        }
    }
    feed_xhttp.open("GET", feed_url, true)
    feed_xhttp.send()
}
document.addEventListener("DOMContentLoaded", () => {
    hltv_el = document.querySelector("#hltv > .info-container-hor")
    rss_el = document.querySelector(".info-block.top-stories > .info-container-hor")
    add_feed(hltv_el, "http://www.hltv.org/news.rss.php", "News")
    add_feed(rss_el, "https://news.ycombinator.com/rss", 'Hacker News')
    add_feed(rss_el, "https://news.google.com/rss", 'Top Stories - US')
})
