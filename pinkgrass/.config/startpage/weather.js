let weather_xhttp = new XMLHttpRequest()

weather_xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        let weather_el = document.getElementById("weather")
        let weather_desc_el = document.createElement("div")
        let weather_json = JSON.parse(weather_xhttp.responseText)
        let city_name = weather_json.nearest_area[0].areaName[0].value
        let region_name = weather_json.nearest_area[0].region[0].value
        let country_name = weather_json.nearest_area[0].country[0].value
        let currently_feels_like = weather_json.current_condition[0].FeelsLikeF
        let current_temp = weather_json.current_condition[0].temp_F
        let weather_desc = weather_json.current_condition[0].weatherDesc[0].value
        
        let fulldesc = "<span class='description'>"
                       + `It is currently <span class="highlight1">${weather_desc}</span> `
                       + `in <span class="highlight2">${city_name}, ${region_name}.</span> `
                       + `The temperature is <span class="highlight3">${current_temp}°F</span> `
                       + `and feels like <span class="highlight4">${currently_feels_like}°F.</span> `
                       + "</span>"

        weather_desc_el.innerHTML = fulldesc
        weather_el.appendChild(weather_desc_el)
    }
};

weather_xhttp.open("GET", "http://wttr.in?format=j1", true)
weather_xhttp.setRequestHeader("User-Agent", "curl/7.37.1")
weather_xhttp.send()
