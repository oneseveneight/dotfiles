function hltv_scores(num_matches) {
    let hltv_xhttp = new XMLHttpRequest()
    hltv_xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let hltv_el = document.querySelector("#hltv > .info-container-hor > .info-block.scrollable.hltv-matches")
            let title_el = document.createElement("h2")
            title_el.className = "minor-title"
            title_el.innerText = "Matches"
            hltv_el.appendChild(title_el)
            match_data = JSON.parse(hltv_xhttp.responseText)
            for (let i = 0; i < num_matches; i++) {
                match = match_data[i + 1]
                event_name = match.event_name
                team_names = match.team_names
                maps_won = match.maps_won
                rounds_won = match.rounds_won
                start_time = match.start_time
                match_status = ""
                if (start_time == "LIVE") {
                    match_status = "live"
                } else if (start_time == "OVER") {
                    match_status = "over"
                } else {
                    match_status = "upcoming"
                }
                
                let infobox_el = document.createElement("div")
                let title_el = document.createElement("p")
                infobox_el.className = "infobox-data hltv-match-" + match_status
                title_el.className = "hltv-event-name"
                title_el.innerText = event_name
                infobox_el.appendChild(title_el)
                if (match_status == "live" ) {
                    for (let j = 0; j < 2; j++) {
                        team_names_el = document.createElement("span")
                        rounds_won_el = document.createElement("span")
                        maps_won_el = document.createElement("span")

                        team_names_el.className = "hltv-team-name"
                        team_names_el.innerText = team_names[j]
                        rounds_won_el.className = "hltv-round-score"
                        rounds_won_el.innerText = rounds_won[j]
                        maps_won_el.className = "hltv-map-score"
                        maps_won_el.innerText = "(" + maps_won[j] + ")"

                        infobox_el.appendChild(team_names_el)
                        infobox_el.appendChild(rounds_won_el)
                        infobox_el.appendChild(maps_won_el)
                        infobox_el.appendChild(document.createElement("br"))
                    }
                } else if (match_status == "upcoming") {
                    for (let j = 0; j < 2; j++) {
                        team_names_el = document.createElement("span")

                        team_names_el.className = "hltv-team-name"
                        team_names_el.innerText = team_names[j]

                        infobox_el.appendChild(team_names_el)
                        infobox_el.appendChild(document.createElement("br"))
                    }
                } else if (match_status == "over") {
                    for (let j = 0; j < 2; j++) {
                        team_names_el = document.createElement("span")
                        maps_won_el = document.createElement("span")

                        team_names_el.className = "hltv-team-name"
                        team_names_el.innerText = team_names[j]
                        maps_won_el.className = "hltv-map-score"
                        maps_won_el.innerText = maps_won[j]

                        infobox_el.appendChild(team_names_el)
                        infobox_el.appendChild(maps_won_el)
                        infobox_el.appendChild(document.createElement("br"))
                    }
                }
                hltv_el.appendChild(infobox_el)
            }
        }
    }
    hltv_xhttp.open("GET", "https://illegaldrugs.net/skor", true)
    hltv_xhttp.send()
}

hltv_scores(20)
