let ipinfo_xhttp = new XMLHttpRequest()

ipinfo_xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        let info_el = document.querySelector(".info-block.ip-info")
        let ip_info = JSON.parse(ipinfo_xhttp.responseText)
        let ip_desc_el = document.createElement("div")

        let fulldesc = "<span class='description'>"
                       + `Your ip is currently <span class="highlight1">${ip_info.ip}</span>, `
                       + `reporting from <span class="highlight2">${ip_info.city}, ${ip_info.region}, ${ip_info.country}</span>, `
                       + `and assigned to <span class="highlight3">${ip_info.org}</span> `
                       + "</span>"
        ip_desc_el.innerHTML = fulldesc
        info_el.appendChild(ip_desc_el)
    }
};

ipinfo_xhttp.open("GET", "https://ipinfo.io", true)
ipinfo_xhttp.setRequestHeader("User-Agent", "curl/7.37.1")
ipinfo_xhttp.send()
