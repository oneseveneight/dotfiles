#!/bin/sh
max_groups="$(cat /home/sose/.config/windowchef/max_groups)"
new_max_groups="$(( max_groups + 1 ))"
waitron wm_config groups_nr $new_max_groups
waitron group_activate_specific $new_max_groups
echo "$new_max_groups" > /home/sose/.config/windowchef/max_groups
