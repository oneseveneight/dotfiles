#!/bin/sh
LOCATION="/home/sose/.config/windowchef/emojimenu"
if [ -z "$1" ]
then
    cat "$LOCATION/emoji_list.txt" \
    | grep " ; " \
    | cut -f2 -d '#' \
    | cut -f2,4- -d ' '
else
    printf "$1" \
    | cut -f1 -d ' '  \
    | tr -d '\n' \
    | xsel -ib
fi
