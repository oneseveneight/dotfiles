#!/bin/sh

if [ -n "$1" ] 
then
    window_id="$(
        echo "$1" \
        | rev \
        | cut -f 1 -d '|' \
        | rev \
        | tr -d ' '
    )"
    current_desktop="$(
        xprop -root _NET_CURRENT_DESKTOP \
        | cut -f 2 -d '=' \
        | tr -d ' '
    )"
    current_desktop=$(( current_desktop + 1 ))

    waitron window_focus "$window_id"
    waitron group_add_window $current_desktop
    exit
fi

window_ids="$(
    xprop -root _NET_CLIENT_LIST \
    | cut -f 2 -d '#' \
    | tr ',' ' ' \
)"
window_name=""

for window_id in $window_ids
do
    window_name="$(
        xprop -id $window_id WM_NAME \
        | cut -f 2 -d '=' \
        | cut -c 2- \
        | tr -d '"'
    )"
    window_names="$window_name | $window_id\n$window_names"
done
echo $window_names
