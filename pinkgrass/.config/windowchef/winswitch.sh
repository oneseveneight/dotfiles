#!/bin/sh

window_id="$1" 
desktop="$(
    xprop -id $window_id _NET_WM_DESKTOP \
    | cut -f 2 -d '='
)"
desktop="$(( desktop + 1 ))"
waitron group_activate_specific "$desktop"
#waitron window_focus "$window_id"
