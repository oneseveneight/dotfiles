static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#cac5c7", "#2c1a20" },
	[SchemeSel] = { "#cac5c7", "#d6497b" },
	[SchemeOut] = { "#cac5c7", "#86cace" },
};
