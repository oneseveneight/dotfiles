static const char norm_fg[] = "#cac5c7";
static const char norm_bg[] = "#2c1a20";
static const char norm_border[] = "#605357";

static const char sel_fg[] = "#cac5c7";
static const char sel_bg[] = "#3399a5";
static const char sel_border[] = "#cac5c7";

static const char urg_fg[] = "#cac5c7";
static const char urg_bg[] = "#d6497b";
static const char urg_border[] = "#d6497b";

static const char *colors[][3]      = {
    /*               fg           bg         border                         */
    [SchemeNorm] = { norm_fg,     norm_bg,   norm_border }, // unfocused wins
    [SchemeSel]  = { sel_fg,      sel_bg,    sel_border },  // the focused win
    [SchemeUrg] =  { urg_fg,      urg_bg,    urg_border },
};
