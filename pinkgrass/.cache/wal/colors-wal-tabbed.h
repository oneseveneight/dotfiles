static const char* selbgcolor   = "#2c1a20";
static const char* selfgcolor   = "#cac5c7";
static const char* normbgcolor  = "#3399a5";
static const char* normfgcolor  = "#cac5c7";
static const char* urgbgcolor   = "#d6497b";
static const char* urgfgcolor   = "#cac5c7";
