const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#2c1a20", /* black   */
  [1] = "#d6497b", /* red     */
  [2] = "#3399a5", /* green   */
  [3] = "#73a9b6", /* yellow  */
  [4] = "#54b9bd", /* blue    */
  [5] = "#e47e9f", /* magenta */
  [6] = "#86cace", /* cyan    */
  [7] = "#cac5c7", /* white   */

  /* 8 bright colors */
  [8]  = "#605357",  /* black   */
  [9]  = "#d6497b",  /* red     */
  [10] = "#3399a5", /* green   */
  [11] = "#73a9b6", /* yellow  */
  [12] = "#54b9bd", /* blue    */
  [13] = "#e47e9f", /* magenta */
  [14] = "#86cace", /* cyan    */
  [15] = "#cac5c7", /* white   */

  /* special colors */
  [256] = "#2c1a20", /* background */
  [257] = "#cac5c7", /* foreground */
  [258] = "#cac5c7",     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
