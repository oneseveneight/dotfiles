#!/bin/sh

fontsize=$(cat ~/.globals | cut -f 2 -d ' ')

batt_status() {
	cat /sys/class/power_supply/PMU_battery_0/status \
	| sed 	-e 's/Charging/\+/' \
		-e 's/Discharging/-/' \
		-e 's/Full//'
}
batt_perc () {
	echo "scale=2; $(cat /sys/class/power_supply/PMU_battery_0/energy_avg) / $(cat /sys/class/power_supply/PMU_battery_0/energy_full)" | bc | tr -d '.'
}
getdate (){
	date '+%a %b %d %Y'
}
gettime (){
	date '+%I:%M %P'
}

while true
do
	echo "%{c} $(getdate) $(gettime)%{r} batt: $(batt_status)$(batt_perc)% "
	sleep 30
done | lemonbar -f "DejaVu Sans Mono-$fontsize"
