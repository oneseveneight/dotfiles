#!/bin/sh
pkill -f 'lemonbar .* current_desktop_name'
bspc query -D -d --names \
	| lemonbar -dpg 13x10+5+2 \
		-f "DejaVu Sans Mono-7.5" \
		-n "current_desktop_name"
