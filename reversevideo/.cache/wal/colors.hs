--Place this file in your .xmonad/lib directory and import module Colors into .xmonad/xmonad.hs config
--The easy way is to create a soft link from this file to the file in .xmonad/lib using ln -s
--Then recompile and restart xmonad.

module Colors
    ( wallpaper
    , background, foreground, cursor
    , color0, color1, color2, color3, color4, color5, color6, color7
    , color8, color9, color10, color11, color12, color13, color14, color15
    ) where

-- Shell variables
-- Generated by 'wal'
wallpaper="/home/sose/Wallpapers/bridge.jpg"

-- Special
background="#1a1919"
foreground="#c5c5c5"
cursor="#c5c5c5"

-- Colors
color0="#1a1919"
color1="#8a684f"
color2="#908174"
color3="#9699a0"
color4="#bd9470"
color5="#a6a9ae"
color6="#d2b087"
color7="#c5c5c5"
color8="#535252"
color9="#8a684f"
color10="#908174"
color11="#9699a0"
color12="#bd9470"
color13="#a6a9ae"
color14="#d2b087"
color15="#c5c5c5"
