static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#c5c5c5", "#1a1919" },
	[SchemeSel] = { "#c5c5c5", "#8a684f" },
	[SchemeOut] = { "#c5c5c5", "#d2b087" },
};
