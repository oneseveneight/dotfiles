const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#1a1919", /* black   */
  [1] = "#8a684f", /* red     */
  [2] = "#908174", /* green   */
  [3] = "#9699a0", /* yellow  */
  [4] = "#bd9470", /* blue    */
  [5] = "#a6a9ae", /* magenta */
  [6] = "#d2b087", /* cyan    */
  [7] = "#c5c5c5", /* white   */

  /* 8 bright colors */
  [8]  = "#535252",  /* black   */
  [9]  = "#8a684f",  /* red     */
  [10] = "#908174", /* green   */
  [11] = "#9699a0", /* yellow  */
  [12] = "#bd9470", /* blue    */
  [13] = "#a6a9ae", /* magenta */
  [14] = "#d2b087", /* cyan    */
  [15] = "#c5c5c5", /* white   */

  /* special colors */
  [256] = "#1a1919", /* background */
  [257] = "#c5c5c5", /* foreground */
  [258] = "#c5c5c5",     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
