set expandtab
set nocompatible
set tabstop=4
set shiftwidth=4
set rnu
colorscheme reversevideo
syntax on
hi Normal ctermfg=black
hi Text ctermbg=white
match Text ".*"
"set laststatus=2
set noruler
set laststatus=0
set noshowcmd
