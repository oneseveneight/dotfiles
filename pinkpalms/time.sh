#!/bin/sh

fontsize="9"
geometry="90x20+1825+5"

gettime (){
	date '+%I:%M %p'
}

while true
do
	echo "%{c}$(gettime)%{r}"
	sleep 30
done | lemonbar -g $geometry -F "#072B3F" -B "#C87CB6" -f "DejaVu Sans Mono-$fontsize"
