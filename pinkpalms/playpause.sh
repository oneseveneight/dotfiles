#!/bin/sh

fontsize="9"
#geometry="150x20+1670+5"
player="spotifyd"
bar_length=""
bar_string=""
nowplaying(){
	if dbus-send --print-reply \
		--session \
		--dest=org.mpris.MediaPlayer2.spotifyd \
		/org/mpris/MediaPlayer2 \
		org.freedesktop.DBus.Properties.Get \
		string:'org.mpris.MediaPlayer2.Player' \
		string:'Metadata' > /dev/null 2>&1 
	then
		SONG=$(dbus-send --print-reply \
			--session \
			--dest=org.mpris.MediaPlayer2.spotifyd \
			/org/mpris/MediaPlayer2 \
			org.freedesktop.DBus.Properties.Get \
			string:'org.mpris.MediaPlayer2.Player' \
			string:'Metadata' | grep title -A 1 |tail -n 1 |cut -c 43-| sed 's/"*"//g')

		ARTIST=$(dbus-send --print-reply \
			--session \
			--dest=org.mpris.MediaPlayer2.spotifyd \
			/org/mpris/MediaPlayer2 \
			org.freedesktop.DBus.Properties.Get \
			string:'org.mpris.MediaPlayer2.Player' \
			string:'Metadata' | grep albumArtist -A 2 | tail -n 1 | cut -c 26- | sed 's/"*"//g')
		STATUS=$(dbus-send --print-reply \
			--session \
			--dest=org.mpris.MediaPlayer2.spotifyd \
			/org/mpris/MediaPlayer2 \
			org.freedesktop.DBus.Properties.Get \
			string:'org.mpris.MediaPlayer2.Player' \
			string:'PlaybackStatus' | tail -n 1)

		bar_string="$ARTIST - $SONG"
		if echo $STATUS | grep "Playing"
		then
			bar_string="▶ $bar_string"
		else
			bar_string="❚❚ $bar_string"
		fi
		bar_length="$(echo "$bar_string" | wc -m)"
		bar_length="$(( bar_length * fontsize ))"
		bar_offset="$(( 1665 - bar_length ))"
	else
		bar_string=""
	fi
}
dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotifyd /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause
nowplaying
pkill -f 'lemonbar .* current_song_name'
geometry="$bar_length""x20+$bar_offset+5"
echo "%{c}$bar_string%{r}" | lemonbar -dpg $geometry -F "#072B3F" -B "#636B99" -f "DejaVu Sans Mono-$fontsize" -n "current_song_name" &

