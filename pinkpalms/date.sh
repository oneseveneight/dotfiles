#!/bin/sh

fontsize="9"
geometry="150x20+1670+5"

getdate (){
	date '+%a %b %d %Y'
}

while true
do
	echo "%{c}$(getdate)%{r}"
	sleep 30
done | lemonbar -g $geometry -F "#072B3F" -B "#5F8FB5" -f "DejaVu Sans Mono-$fontsize"
